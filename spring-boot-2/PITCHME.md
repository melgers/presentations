# Spring Boot Glimmen

Interactieve sessie

note:
Pratische introductie tot spring boot.
---

### Spring heel in het kort

-  Onderdeel van Pivotal
-  Bestaat sinds 2003
-  Begonnen als Inversion of Control (IoC) container
-  Alternatief voor JEE (toen J2EE)
-  Eerst beans.xml later ook @nnotaties

note:
Korte introductie tot spring. Geen new SomeObject() maar configuratie buiten code. Spring Boot is een extentie van Spring. Spring is prima te gebruiken zonder spring boot. Zie bijvoorbeeld het Apache Camel project. Vroeger bestond een spring context uit een beans.xml. Tegenwoording worden meestal annotaties gebruikt.
---
## beans.xml, annotations, groovy
```java
@Configuration
public class AppConfig {
 @Bean
 public MyBean myBean() {
     // instantiate, configure and return bean ...
 }
}
```
```xml
<bean id="..." class="...">
    <property name="isolation">
        <util:constant static-field="java.sql.Connection.TRANSACTION_SERIALIZABLE"/>
    </property>
</bean>
```
```groovy
beans {
    framework String, 'Grails'
    foo String, 'hello'
    bar(Bar,s:'hello',i:123)
}
```
---

### Templates 

- Templates maken bestaande API's eenvoudiger
- [JBDC Template](https://docs.spring.io/spring/docs/4.3.9.RELEASE/spring-framework-reference/html/jdbc.html#jdbc-JdbcTemplate)
- [JMS Template](https://docs.spring.io/spring/docs/4.3.9.RELEASE/spring-framework-reference/html/jms.html)
- RabbitMQ
- [Email Template](https://docs.spring.io/spring/docs/4.3.9.RELEASE/spring-framework-reference/html/mail.html#mail)
- Freemarker, Velocity, Thymeleaf 
- Etc...

note:
Het spring team heeft aangegeven bestaande technieken die goed zijn niet opnieuw uit te vinden maar het gebruik ervan eenvoudiger te maken.
---

### Frameworks

- [Spring Security](https://docs.spring.io/spring-security/site/docs/4.2.7.RELEASE/reference/htmlsingle/)
- [String Caching](https://docs.spring.io/spring/docs/4.3.9.RELEASE/spring-framework-reference/html/cache.html#cache-jsr-107)
- Spring Web
- [Spring Data](http://projects.spring.io/spring-data/)
- Spring Batch
- Spring Cloud
- Spring Integration
- Many more. 

note:
Naast templates ook hele frameworks. 
---
## Spring Boot

- Alleen Spring kan **alles** wat Spring Boot kan
- **Voordeel** : Minder jars
- **Nadeel** : Meer plumbing
---
## Spring Boot

- Spring Boot is onderdeel spring ecosysteem
- Eenvoudig applicaties bootstrappen
- Maakt gebruik van verschillende Spring Frameworks
- Automatische configuratie (convention over configuration)
- Modules worden als maven dependencies toegevoegd middels 'starter' modules
---
## pom.xml

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>

<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-devtools</artifactId>
	<scope>runtime</scope>
</dependency>
```
---
## Configuratie

- @SpringBootApplication
- Leuk artikel in java magazine 3 - 2018
- Scan of classes bepaalde interfaces implementeren
- [conditional config](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/autoconfigure/condition/package-frame.html)
---
## Spring Boot Admin server

- [Admin Server](http://codecentric.github.io/spring-boot-admin/2.0.2/#getting-started)
- [Demo](http://localhost:8090)
- [Status jmx demo](http://localhost:8080/status)
- [Logger demo](http://localhost:8080/loggerdemo)
---
## Integration with Spring Cloud
- [Netflix](https://cloud.spring.io/spring-cloud-netflix/)
- Config Server
- Zipkin ( logging /tracing )
- Eureka ( discovery server)
- Zuul ( gateway server)

note:
Open zipkin is een spring boot service. Ooo Netflix heeft een groot deel van zijn ecosysteem op spring boot gebaseerd. Eureka en Zuul zijn spring boot services.
---

## Testen

-  Testing is integrated into spring boot.
-  JPA, JDBC, Mongo
-  Draaiende applicatie maar ook mock
---
## Spring Security

-   [Role based URL beveiliging](https://docs.spring.io/spring-security/site/docs/4.2.8.BUILD-SNAPSHOT/reference/htmlsingle/#hello-web-security-java-configuration)
-   [Role based method beveiling](https://docs.spring.io/spring-security/site/docs/4.2.8.BUILD-SNAPSHOT/reference/htmlsingle/#jc-method)
-   [Domain Object Security](https://docs.spring.io/spring-security/site/docs/4.2.8.BUILD-SNAPSHOT/reference/htmlsingle/#domain-acls)

---

## Spring Data
---

## Spring Data JPA

---
## Spring Security
---
```java
interface PersonRepository extends Repository<User, Long> {

  List<Person> findByEmailAddressAndLastname(EmailAddress emailAddress, String lastname);

  // Enables the distinct flag for the query
  List<Person> findDistinctPeopleByLastnameOrFirstname(String lastname, String firstname);
  List<Person> findPeopleDistinctByLastnameOrFirstname(String lastname, String firstname);

  // Enabling ignoring case for an individual property
  List<Person> findByLastnameIgnoreCase(String lastname);
  // Enabling ignoring case for all suitable properties
  List<Person> findByLastnameAndFirstnameAllIgnoreCase(String lastname, String firstname);

  // Enabling static ORDER BY for a query
  List<Person> findByLastnameOrderByFirstnameAsc(String lastname);
  List<Person> findByLastnameOrderByFirstnameDesc(String lastname);
}
```
---

