## GitPitch presentations

[Spring Boot](https://gitpitch.com/melgers/presentations/master?grs=gitlab&t=black&p=spring-boot)

[Spring Boot (kleine aanpassing)](https://gitpitch.com/melgers/presentations/master?grs=gitlab&t=black&p=spring-boot-2#/)

[Programmeren Intro](https://gitpitch.com/melgers/presentations/master?grs=gitlab&p=programmeren-intro)

[Scratch Doolhof](https://gitpitch.com/melgers/presentations/master?grs=gitlab&t=night&p=scratch-doolhof)

## GitPitch handy docs

[GITPITCH.yaml settings](https://github.com/gitpitch/gitpitch/wiki/Slideshow-Settings)

[GITPITCH asset sharing](https://github.com/gitpitch/gitpitch/wiki/Asset-Sharing)