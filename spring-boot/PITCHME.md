# Spring Boot

Practische introductie

note:
Pratische introductie tot spring boot.
---

### Agenda

- Eten
- Spring Introductie(15 Min)
- Workshop deel 1 (~45 min)
- pauze (15 min)
- Workshop deel 2 (~45 min)

---

### Spring heel in het kort

-  Onderdeel van Pivotal
-  Bestaat sinds 2003
-  Begonnen als Inversion of Control (IoC) container
-  Alternatief voor JEE (toen J2EE)
-  Eerst beans.xml later ook @nnotaties

note:
Korte introductie tot spring. Geen new SomeObject() maar configuratie buiten code. Spring Boot is een extentie van Spring. Spring is prima te gebruiken zonder spring boot. Zie bijvoorbeeld het Apache Camel project. Vroeger bestond een spring context uit een beans.xml. Tegenwoording worden meestal annotaties gebruikt.
---
## beans.xml & annotations
```java
@Configuration
public class AppConfig {

 @Bean
 public MyBean myBean() {
     // instantiate, configure and return bean ...
 }

}
```

```xml
<bean id="..." class="...">
    <property name="isolation">
        <util:constant static-field="java.sql.Connection.TRANSACTION_SERIALIZABLE"/>
    </property>
</bean>
```
---

### Templates 

- Templates maken bestaande API's eenvoudiger
- JBDC Template
- JMS Template
- RabbitMQ
- Email Template
- Freemarker, Velocity, Thymeleaf 
- Etc...

note:
Het spring team heeft aangegeven bestaande technieken die goed zijn niet opnieuw uit te vinden maar het gebruik ervan eenvoudiger te maken.
---

### Frameworks

- Spring Security
- Spring Web
- Spring Data
- Spring Batch
- Spring cloud
- Spring Integration
- Many more. 

note:
Naast templates ook hele frameworks. 
---
## Spring Boot

- Spring Boot is onderdeel spring ecosysteem
- Eenvoudig applicaties bootstrappen
- Maakt gebruik van verschillende Spring Frameworks
- Automatische configuratie (convention over configuration)
- Modules worden als maven dependencies toegevoegd middels 'starter' modules
---
## pom.xml

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>

<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-devtools</artifactId>
	<scope>runtime</scope>
</dependency>
```
---

## Integration with Spring Cloud

- Config Server
- Zipkin ( logging /tracing )
- Eureka ( discovery server)
- Zuul ( gateway server)

note:
Open zipkin is een spring boot service. Ooo Netflix heeft een groot deel van zijn ecosysteem op spring boot gebaseerd. Eureka en Zuul zijn spring boot services.
---

## Testen

-  Testing is integrated into spring boot.
-  JPA, JDBC, Mongo
-  Draaiende applicatie maar ook mock

---

## Workshop

`git clone https://gitlab.flusso.nl/hmelgers/spring-boot-workshop`

Volg stappen in NOTES.md

---
## Workshop part 1 

- Spring Boot App aanmaken en opstarten
- Spring Boot Actuator
- Profiles
- Add a controller
- Configuration from service (optional)
---
## Workshop part 2

- Add starter data jpa
- Add templating support
- Distributed tracing with zipkin
- Keycloak en Spring security
- Testing
- Spring Data Rest
---

## Spring Data JPA

```java
interface PersonRepository extends Repository<User, Long> {

  List<Person> findByEmailAddressAndLastname(EmailAddress emailAddress, String lastname);

  // Enables the distinct flag for the query
  List<Person> findDistinctPeopleByLastnameOrFirstname(String lastname, String firstname);
  List<Person> findPeopleDistinctByLastnameOrFirstname(String lastname, String firstname);

  // Enabling ignoring case for an individual property
  List<Person> findByLastnameIgnoreCase(String lastname);
  // Enabling ignoring case for all suitable properties
  List<Person> findByLastnameAndFirstnameAllIgnoreCase(String lastname, String firstname);

  // Enabling static ORDER BY for a query
  List<Person> findByLastnameOrderByFirstnameAsc(String lastname);
  List<Person> findByLastnameOrderByFirstnameDesc(String lastname);
}
```
---

