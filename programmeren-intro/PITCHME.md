### Programmeren

-  Het schrijven van een computer programma
-  Web sites
-  Games
-  Programma's

---
### Scratch

-  Wij gaan programmeren in scratch

![Scratch](programmeren-intro/images/scratch.png)

---
### Dash & Dot

-  Vanaf volgende week

![Scratch](programmeren-intro/images/dash-en-dot.png)

---
### Wat gaan we doen

-   Week 1, allemaal scratch leren kennen
-   Week 2, groep 1 *doolhof game*, groep 2 *dash en dot*
-   Week 3, groep 2 *doolhof game*, groep 1 *dash en dot*

---
### Op Scratch inloggen

-  Website : https://scratch.mit.edu 
-  Mirrin , leerling1001 , corlaer1
-  Jurre , leerling1002 , corlaer1
-  Sophie , leerling1003 , corlaer1
-  Jeftha , leerling1004 , corlaer1
-  Sarah , leerling1005 , corlaer1
-  Sander , leerling1006 , corlaer1
-  Ties , leerling1007 , corlaer1
-  Jesper , leerling1008 , corlaer1
-  Aya , leerling1009 , corlaer1
-  Rozemarijn , leerling1009 , corlaer1

---
### Korte uitleg scratch

![Stage](programmeren-intro/images/scratch-panels.png)

---
### Stage

![Stage](programmeren-intro/images/scratch-stage.png)

---
### Sprites

![Sprites](programmeren-intro/images/sprites-panel.jpg)

---
### Scripts

![Script](programmeren-intro/images/scratch-script.png)

---
### Opdrachten achter elkaar

Opdracht : tel tot 8

![Script](programmeren-intro/images/code-achter-elkaar.png)

---
### Maar wat als de kat tot 100 moet tellen?
---
### Of als de kat tot 1000 moet tellen? 
---
### Opdrachten herhalen

Opdracht : tel tot 100

![Script](programmeren-intro/images/herhalen.png)
![Script](programmeren-intro/images/code-herhalen.png)

---
### Beslissingen nemen

-  Als op de spatiebalk gedrukt wordt zeg "au!"
![Script](programmeren-intro/images/als-dan.png)
![Script](programmeren-intro/images/code-als-dan.png)
---
### Lekker beginnen!

-  Is scratch nieuw voor je? Pak startersboekje
-  Ken je scratch al, pak dan gekleurde opdrachtkaarten
-  Vraag Hans, Sander of Hans om hulp
-  Help elkaar
-  Rust is belangrijk bij programmeren

