
### Geen YouTube vandaag
---

---?image=https://gitlab.com/melgers/presentations/raw/master/scratch-doolhof/images/scratch-doolhof.png&size=auto

---
### Sprites

![Sprites](programmeren-intro/images/sprites-panel.jpg)

---
### Scripts

![Script](programmeren-intro/images/scratch-script.png)

---
### Opdrachten achter elkaar

Opdracht : tel tot 8

![Script](programmeren-intro/images/code-achter-elkaar.png)

---
### Opdrachten herhalen

Opdracht : tel tot 100

![Script](programmeren-intro/images/herhalen.png)
![Script](programmeren-intro/images/code-herhalen.png)

---
### Beslissingen nemen

-  Als op de spatiebalk gedrukt wordt zeg "au!"
![Script](programmeren-intro/images/als-dan.png)
![Script](programmeren-intro/images/code-als-dan.png)

---
### Iets onthouden (variabelen)

-  Soms moet een programma iets onthouden
-  Hiervoor kun je bakjes maken om iets in te stoppen
-  Zo'n bakje heet een variabele
---
### Variabelen voorbeeld

![Script](programmeren-intro/images/variabelen.png)

---
### Doolhof

Als eerste hebben we een doolhof nodig voor ons katje.

![Maze generator](scratch-doolhof/images/maze-generator.png)

---

### Doolhof maken

1. Ga naar [www.mazegenerator.net](http://www.mazegenerator.net/)

2. Stel Width en Heigth beide in op 15

3. Klik op `Generate new` (Dit kan net zolang tot je een leuk doolhof ziet)

4. Klik op `Download` om het doolhof op slaan. 

5. We kunnen deze nu gebruiken in ons scratch project.

---

### Importeer je doolhof als sprite in je project

-  Importeer je doolhof door bij sprites op het mapje te klikken.

---
### Voeg een visje toe

-  Je katje wil een visje.
-  Voeg een visje toe aan je project. 

---
### Maak twee variabelen bij data

-  Selecteer bij *scripts* het oranje *Data* blokje
-  Een variabele is een soort bakje waar je een waarde aan kunt geven.
-  Maak een variabele met de naam *xnu*
-  Maak een variabele met de naam *ynu*

---
### Deel 1 om het programma te maken

-  Zet je katje bij de ingang van je doolhof
-  Zet je visje bij de uitgang van je doolhof
-  Zorg dat je het katje weer gekozen hebt
-  Zet je katje bij *uiterlijken* op *ga naar voorgrond*
-  Voeg bij *besturen* een *herhaal tot*
-  Zet in het *herhaal tot* een *Waarnemen raak ik fish1*

---
### Deel 2 om het programma te maken

-  We gaan ons katje laten lopen
-  Voeg van *besturen* een *als* toe
-  Zet in deze *als* een *waarnemen toets pijlje omhoog toe*
-  Zet binnen de *als* een *bewegen verander y met 4*
-  Zet in deze *als* een *waarnemen toets pijlje omlaag toe*
-  Zet binnen de *als* een *bewegen verander y met -4*

---
### Deel 3 om het programma te maken

-  Zet in deze *als* een *waarnemen toets pijlje links toe*
-  Zet binnen de *als* een *bewegen verander x met -4*
-  Zet in deze *als* een *waarnemen toets pijlje links toe*
-  Zet binnen de *als* een *bewegen verander x met 4*
-  Ons katje kan nu lopen

---
### Niet door muren

-  

